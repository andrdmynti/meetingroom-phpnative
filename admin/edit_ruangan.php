<?php
  
  include '../config/connection.php';

  $id_ruangan = $_GET['id_ruangan'];

  $query      = "SELECT * FROM ruangan WHERE id_ruangan='$id_ruangan'";
  $select     = mysqli_query($conn, $query)or die(mysqli_error($conn));
  $data    = mysqli_fetch_array($select);


?>
<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="index.php">Dashboard</a>
  </li>
  <li class="breadcrumb-item">
    <a href="index.php?halaman=manage_ruangan">Management Rooms</a>
  </li>
  <li class="breadcrumb-item active">Edit Rooms</li>
</ol>
<br><br>

<div class="card mb-3">
  <div class="card-header">
      <i class="fa fa-sticky-note"></i> Management Ruangan
  </div>
  <div class="card-body">
    <form action="../config/UpdateRooms.php" method="POST">
      <input type="hidden" name="id_ruangan" value="<?php echo $id_ruangan ?>">
        <div class="form-group row">
            <label class="col-4 col-form-label">Nama Ruangan</label>
          <label class="col-1 col-form-label">:</label>
          <div class="col-6">
            <input class="form-control" name="nama" type="text" value="<?php echo $data['nama_ruangan'] ?>">
          </div>
        </div>
        <div class="form-group row">
            <label class="col-4 col-form-label">Tata Letak Ruangan</label>
          <label class="col-1 col-form-label">:</label>
          <div class="col-6">
            <img src="<?php echo $data['tata_letak'] ?>">
<!--             <input class="form-control" name="letak" type="text" value="<?php echo $data['tata_letak'] ?>"> -->
          </div>
        </div>
        <div class="form-group row">
            <label class="col-4 col-form-label">Kapasitas Ruangan</label>
          <label class="col-1 col-form-label">:</label>
          <div class="col-6">
            <input class="form-control" name="kapasitas" type="text" value="<?php echo $data['kapasitas'] ?>">
          </div>
        </div>
        <div class="form-group row">
            <label class="col-4 col-form-label">Fasilitas Ruangan</label>
          <label class="col-1 col-form-label">:</label>
          <div class="col-6">
            <textarea class="form-control" name="fasilitas" type="text">
              <?php echo $data['fasilitas']?>
            </textarea>
          </div>
        </div>
        <div class="form-group row">
            <label class="col-4 col-form-label"></label>
            <label class="col-1 col-form-label"></label>
            <div class="col-6" align="Right">
              <button class="btn btn-danger">Update</button>
          </div>
        </div>
    </form>
  </div>
</div>