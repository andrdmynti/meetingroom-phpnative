<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="">Dashboard</a>
  </li>
  <li class="breadcrumb-item active">
  	Rooms
  </li>
</ol>
<br>
<br>
<div class="row">
	<div class="col-sm-4">
		
	</div>
	<div class="col-sm-4">
		
	</div>
	<div class="col-sm-3" align="right">
			<?php
			
				date_default_timezone_set('Asia/Jakarta');

				$tgl	= date('Y-m-d');
				$jam 	= date('G:i');

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

			?>		
	</div>
	<div class="col-sm-1">
		
	</div>
</div>
<div class="card mb-3">
	<div class="card-body">
		
		<div class="row" align="center">
			<!-- RUANGAN BOARD ROOM 1 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE ruangan='Board 1'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '

				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="../ruangan/board1.php">
				    				<img src="../photos/occup_board1.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {
						echo '
							<div class="col-sm-4">
							<br><br>
								<a href="../ruangan/board1.php">
									<img src="../photos/avail_board1.png">;		
								</a>
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}

			?>
			<!-- 1RUANGAN BOARD ROOM 2 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Board 2'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '

				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="../ruangan/board2.php">
				    				<img src="../photos/occup_board2.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '
							<div class="col-sm-4">
							<br><br>
								<a href="../ruangan/board2.php">
									<img src="../photos/avail_board2.png">;		
								</a>
							</div>
							';

						
					}
					
				} catch (Exception $e) {
					echo $e;
				}

			?>
			<!-- RUANGAN PPP 1 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='PPP 1'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="../ruangan/ppp1.php">
				    				<img src="../photos/occup_ppp1.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '
							<div class="col-sm-4">
							<br><br>
								<a href="../ruangan/ppp1.php">
									<img src="../photos/avail_ppp1.png">;		
								</a>
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}

			?>
			<!-- RUANGAN PPP 2 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='PPP 2'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="../ruangan/ppp2.php">
				    				<img src="../photos/occup_ppp2.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '
							<div class="col-sm-4">
							<br><br>
								<a href="../ruangan/ppp2.php">
									<img src="../photos/avail_ppp2.png">;		
								</a>
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}

			?>
			<!-- RUANGAN PPP 3 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='PPP 3'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="../ruangan/ppp3.php">
				    				<img src="../photos/occup_ppp3.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '
							<div class="col-sm-4">
							<br><br>
								<a href="../ruangan/ppp3.php">
									<img src="../photos/avail_ppp3.png">;		
								</a>
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}

			?>
			<!-- RUANGAN PPP 4 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='PPP 4'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="../ruangan/ppp4.php">
				    				<img src="../photos/occup_ppp4.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<a href="../ruangan/ppp4.php">
									<img src="../photos/avail_ppp4.png">;		
								</a>
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}

			?>
			<!-- RUANGAN MEZZANINE 1 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Mezzanine 1'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="../ruangan/mezzanine1.php">
				    				<img src="../photos/occup_mezzanine1.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<a href="../ruangan/mezzanine1.php">
									<img src="../photos/avail_mezzanine1.png">;		
								</a>
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}
			?>
			<!-- RUANGAN MEZZANINE 2 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Mezzanine 2'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="../ruangan/mezzanine2.php">
				    				<img src="../photos/occup_mezzanine2.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<a href="../ruangan/mezzanine2.php">
									<img src="../photos/avail_mezzanine2.png">;		
								</a>
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}
			?>
			<!-- RUANGAN MEZZANINE 3 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Mezzanine 3'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="../ruangan/mezzanine3.php">
				    				<img src="../photos/occup_mezzanine3.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<a href="../ruangan/mezzanine3.php">
									<img src="../photos/avail_mezzanine3.png">;		
								</a>
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}
			?>
			<!-- RUANGAN BOROBUDUR -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Borobudur'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="../ruangan/borobudur.php">
				    				<img src="../photos/occup_borobudur.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<a href="../ruangan/borobudur.php">
									<img src="../photos/avail_borobudur.png">;		
								</a>
							</div>';			
					}
				} catch (Exception $e) {
					echo $e;
				}
			?>
			<!-- RUANGAN PRAMBANAN -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Prambanan'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="../ruangan/prambanan.php">
				    				<img src="../photos/occup_prambanan.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<a href="../ruangan/prambanan.php">
									<img src="../photos/avail_prambanan.png">;		
								</a>
							</div>';			
					}
				} catch (Exception $e) {
					echo $e;
				}
			?>
			<!-- RUANGAN MENDUT -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Mendut'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="../ruangan/mendut.php">
				    				<img src="../photos/occup_mendut.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<a href="../ruangan/mendut.php">
									<img src="../photos/avail_mendut.png">;		
								</a>
							</div>';			
					}
				} catch (Exception $e) {
					echo $e;
				}
			?>
			<!-- RUANGAN PAWON -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Pawon'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		</div>
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="../ruangan/pawon.php">
				    				<img src="../photos/occup_pawon.png">;		
				    			</a>
				    		</div>
				    		<div class="col-sm-4">
				    		</div>
				    	';
				    
				    } else {

						echo '
				    		<div class="col-sm-4">
				    		</div>	
							<div class="col-sm-4">
							<br><br>
								<a href="../ruangan/pawon.php">
									<img src="../photos/avail_pawon.png">;		
								</a>
							</div>
				    		<div class="col-sm-4">
				    		</div>';			
					}
				} catch (Exception $e) {
					echo $e;
				}
			?>
		</div>
	</div>
</div>