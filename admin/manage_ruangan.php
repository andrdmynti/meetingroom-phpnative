<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="index.php">Dashboard</a>
  </li>
  <li class="breadcrumb-item active">Management Rooms</li>
</ol>
<br>
<a href="index.php?halaman=insert_ruangan"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Insert Rooms</button></a>
<br><br>
<div class="card mb-3">
	<div class="card-header">
    	<i class="fa fa-sticky-note"></i> Management Rooms
	</div>
  <div class="card-body">
  	<div class="table-responsive">
  		<table class="table table-striped" width="100%" cellspacing="0">
  	    	<thead>
  	      		<tr>
  	        		<th>No</th>
  	        		<th>Nama Ruangan</th>
                <th>Tata Letak</th>
                <th>Kapasitas</th>
  	        		<th>Fasilitas</th>
  	        		<th><center>Action</center></th>
  	      		</tr>
  	    	</thead>
  	    	<tbody>
            <?php 
              include '../config/connection.php';

              $query = mysqli_query($conn, "SELECT * FROM ruangan")or die(mysqli_error($conn));
                    if(mysqli_num_rows($query) == 0){
                echo '<tr><td colspan="5" align="center">Tidak ada data!</td></tr>';
              }
              else {
                $no = 1;
                while($data = mysqli_fetch_array($query)){  
                  echo '<tr>';
                  echo '<td>'.$no.'</td>';
                  echo '<td>'.$data['nama_ruangan'].'</td>';
                  echo '<td><img src="'.$data['tata_letak'].'" width="200px" height="100px"></td>';
                  echo '<td>'.$data['kapasitas'].'</td>';
                  echo '<td>'.$data['fasilitas'].'</td>';
                  echo '<td>
                    <center>
                       <a href="index.php?halaman=edit_ruangan&&id_ruangan='.$data['id_ruangan'].'">
                         <button type="button" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> 
                           Edit  
                         </button>
                       </a>
                       <a href="../config/deleteRooms.php?id_ruangan='.$data['id_ruangan'].'" onclick="ConfirmDelete()"><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button>
                      </a>
                    </center>';
                  $no++;
                 }
                }
            ?>
  	    	</tbody>
  	  	</table>
  	 </div>
  </div>
</div>

<script type="text/javascript">
  function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete ?");
      if(x)
        return true;
      else
        return false;
    }   
</script>
