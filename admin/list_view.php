<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="index.php?halaman=dashboard">Dashboard</a>
  </li>
  <li class="breadcrumb-item active">
    List View
  </li>
</ol>
<br>
<div class="row">
  <div class="col-sm-4">
      
    </div>
    <div class="col-sm-4">
      
    </div>
    <div class="col-sm-3" align="right">
      <?php

          echo "Welcome, <b>".$_SESSION['nama']."</b>";
          echo "<br>";
          date_default_timezone_set('Asia/Jakarta');
          echo date('D, d M Y');
          echo "<br>";
          $tgl  = date('Y-m-d');
          $jam  = date('G:i');

          echo $jam;
          echo "<br><br>";
          
      ?>
    </div>  
</div>

<!-- <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Insert Content</button></a> -->
<div class="card mb-3">
	<div class="card-header">
		<center><a href="index.php"> Icon View</a> / <a active>List View</a></center>
	</div>
  <div class="card-body">
    	<div class="table-responsive">
    		<table class="table table-striped" width="100%" cellspacing="0">
    	    	<thead>
    	      		<tr>
    	        		<th>No</th>
    	        		<th>Room</th>
    	        		<th>Activity</th>
    	      		</tr>
    	    	</thead>
    	    	<tbody>
              <?php 
                include '../config/connection.php';

                $query = mysqli_query($conn, "SELECT nama_ruangan FROM ruangan")or die(mysqli_error($conn));
                      if(mysqli_num_rows($query) == 0){
                  echo '<tr><td colspan="5" align="center">Tidak ada data!</td></tr>';
                }
                else {
                  $no = 1;
                  while($data = mysqli_fetch_array($query)){  
                    echo '<tr>';
                    echo '<td>'.$no.'</td>';
                    echo '<td>'.$data['nama_ruangan'].'</td>';
              ?>
                    <td>
                      <?php
                        $start = strtotime($tampil['start']);
                        $end   = strtotime($tampil['finish']);

                        try {

                          if (strtotime($jam) >= $start && strtotime($jam) < $end) {
                              echo 'Sedang Digunakan';
                            
                            } else {

                            echo '';      
                          }
                        } catch (Exception $e) {
                          echo $e;
                        }
                      ?>
                    </td>
                    <!-- // echo '<td>'.$data['activity'].'</td>'; -->
                  <?php
                    $no++;
                   }
                  }
              ?>
    	    	</tbody>
    	  	</table>
    	 </div>
    </div>
</div>