<br>
<br>
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="index.php">Dashboard</a>
  </li>
  <li class="breadcrumb-item">
    <a href="index.php?halaman=manage_ruangan">Rooms</a>
  </li>
  <li class="breadcrumb-item active">Details Activity Rooms</li>
</ol>
<!-- <?php
    
    include 'config/connection.php';

    $ruangan = $_GET['ruangan'];

    $query      = "SELECT nama, rapat, divisi, ruangan, tanggal, start, finish FROM reservation WHERE ruangan = '$ruangan'";
    $select     = mysqli_query($conn, $query)or die(mysqli_error($conn));
    $hasil      = mysqli_fetch_array($select);

?> -->
<br>
<br>
<div class="card mb-3">
    <div class="card-header">
        <center>
            <h5>
                <b>
                    <?php
                        if ($hasil['ruangan']=='Board1')
                            echo 'Board 1';
                        elseif ($hasil['ruangan']=='Board2')
                            echo 'Board 2';
                        elseif ($hasil['ruangan']=='PPP1')
                            echo 'PPP 1';
                        elseif ($hasil['ruangan']=='PPP2')
                            echo 'PPP 2';
                        elseif ($hasil['ruangan']=='PPP3')
                            echo 'PPP 3';
                        elseif ($hasil['ruangan']=='PPP4')
                            echo 'PPP 4';
                        elseif ($hasil['ruangan']=='Mezzanine1')
                            echo 'Mezzanine 1';
                        elseif ($hasil['ruangan']=='Mezzanine2')
                            echo 'Mezzanine 2';
                        elseif ($hasil['ruangan']=='Mezzanine3')
                            echo 'Mezzanine 3';
                        else echo $hasil['ruangan'];
                    ?>
                </b>
            </h5>
        </center>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <b>Subject</b>
            </div>
            <div class="col-sm-8">
                
            </div>
            <div class="col-sm-4">
                <center><?php echo $hasil['rapat'] ?></center>
            </div>
            <div class="col-sm-8">
                
            </div>
            <div class="col-sm-4">
                <br><br>
                <b>Time</b>
            </div>
            <div class="col-sm-8">
                
            </div>
            <div class="col-sm-4">
                <center><?php echo $hasil['start'] ?> - <?php echo $hasil['finish']; ?></center>
                <br>
                <br>
            </div>
            <div class="col-sm-8">
                
            </div>
            <div class="col-sm-4">
                <b>Requester</b>
            </div>
            <div class="col-sm-8">
                
            </div>
            <div class="col-sm-4">
              <center>
                <p><?php echo $hasil['nama'] ?></p>
                <p><?php echo $hasil['divisi'] ?></p>
                <p>zxzxzczxzxczc</p>
              </center>
            </div>
        </div>
    </div>
</div>