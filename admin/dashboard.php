<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item active">
    Dashboard
  </li>
</ol>
<br>
<div class="row">
	<div class="col-sm-4">
		
	</div>
	<div class="col-sm-4">
		
	</div>
	<div class="col-sm-3" align="right">
		<?php
			
			echo "Welcome, <b>".$_SESSION['nama']."</b>";
			echo "<br>";
			date_default_timezone_set('Asia/Jakarta');
			echo date('D, d M Y');
			echo "<br>";
			$tgl	= date('Y-m-d');
			$jam 	= date('G:i');

			echo $tgl;
			echo "<br>";
			/*echo $jam;
			echo "<br><br>";*/

			/*$query      = "SELECT * FROM reservation WHERE tanggal='$tgl'";
			$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
			$tampil     = mysqli_fetch_array($select);
*/
		?>		
	</div>
	<div class="col-sm-1">
		
	</div>
</div>
<div class="card mb-3">
	<div class="card-header">
		<center><a active> Icon View</a> / <a href="index.php?halaman=list_view">List View</a></center>
	</div>
	<div class="card-body">
		<br>
		<div class="row" align="center">
			<!-- BOARD ROOM 1 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE ruangan='Board1' AND tanggal='$tgl'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);


				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '

				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="index.php?halaman=detail_activity&&ruangan=Board1">
				    				<img src="../photos/occup_board1.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {
						echo '
							<div class="col-sm-4">
							<br><br>
								<img src="../photos/avail_board1.png">;		
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}

			?>
			<!-- BOARD ROOM 2 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Board2'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '

				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="index.php?halaman=detail_activity&&ruangan=Board2">
				    				<img src="../photos/occup_board2.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '
							<div class="col-sm-4">
							<br><br>
								<img src="../photos/avail_board2.png">;		
							</div>
							';

						
					}
					
				} catch (Exception $e) {
					echo $e;
				}

			?>
			<!-- PPP 1 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE ruangan='PPP1' AND tanggal='$tgl'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="index.php?halaman=detail_activity&&ruangan=PPP1">
				    				<img src="../photos/occup_ppp1.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '
							<div class="col-sm-4">
							<br><br>
								<img src="../photos/avail_ppp1.png">;
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}

			?>
			<!-- PPP 2 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='PPP2'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="index.php?halaman=detail_activity&&ruangan=PPP2">
				    				<img src="../photos/occup_ppp2.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '
							<div class="col-sm-4">
							<br><br>
								<img src="../photos/avail_ppp2.png">;
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}

			?>
			<!-- PPP 3 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='PPP3'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="index.php?halaman=detail_activity&&ruangan=PPP3">
				    				<img src="../photos/occup_ppp3.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '
							<div class="col-sm-4">
							<br><br>
								<img src="../photos/avail_ppp3.png">;
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}

			?>
			<!-- PPP 4 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='PPP4'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="index.php?halaman=detail_activity&&ruangan=PPP4">
				    				<img src="../photos/occup_ppp4.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<img src="../photos/avail_ppp4.png">;
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}

			?>
			<!-- MEZZANINE 1 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Mezzanine1'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="index.php?halaman=detail_activity&&ruangan=Mezzanine1">
				    				<img src="../photos/occup_mezzanine1.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<img src="../photos/avail_mezzanine1.png">;
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}
			?>
			<!-- MEZZANINE 2 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Mezzanine2'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="index.php?halaman=detail_activity&&ruangan=Mezzanine2">
				    				<img src="../photos/occup_mezzanine2.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<img src="../photos/avail_mezzanine2.png">;
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}
			?>
			<!-- MEZZANINE 3 -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Mezzanine 3'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="index.php?halaman=detail_activity&&ruangan=Mezzanine3">
				    				<img src="../photos/occup_mezzanine3.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<img src="../photos/avail_mezzanine3.png">;
							</div>';
						
					}
					
				} catch (Exception $e) {
					echo $e;
				}
			?>
			<!-- BOROBUDUR -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Borobudur'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="index.php?halaman=detail_activity&&ruangan=Borobudur">
				    				<img src="../photos/occup_borobudur.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<img src="../photos/avail_borobudur.png">;
							</div>';			
					}
				} catch (Exception $e) {
					echo $e;
				}
			?>
			<!-- PRAMBANAN -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Prambanan'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="index.php?halaman=detail_activity&&ruangan=Prambanan">
				    				<img src="../photos/occup_prambanan.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<img src="../photos/avail_prambanan.png">;
							</div>';			
					}
				} catch (Exception $e) {
					echo $e;
				}
			?>
			<!-- MENDUT -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Mendut'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="index.php?halaman=detail_activity&&ruangan=Mendut">
				    				<img src="../photos/occup_mendut.png">;		
				    			</a>
				    		</div>
				    	';
				    
				    } else {

						echo '	
							<div class="col-sm-4">
							<br><br>
								<img src="../photos/avail_mendut.png">;		
							</div>';			
					}
				} catch (Exception $e) {
					echo $e;
				}
			?>
			<!-- PAWON -->
			<?php

				$query      = "SELECT * FROM reservation WHERE tanggal='$tgl' AND ruangan='Pawon'";
				$select     = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$tampil     = mysqli_fetch_array($select);

				$start = strtotime($tampil['start']);
				$end   = strtotime($tampil['finish']);

				try {

					if (strtotime($jam) >= $start && strtotime($jam) < $end) {
				    	echo '
				    		<div class="col-sm-4">
				    		</div>
				    		<div class="col-sm-4">
				    		<br><br>
				    			<a href="index.php?halaman=detail_activity&&ruangan=Pawon">
				    				<img src="../photos/occup_pawon.png">;		
				    			</a>
				    		</div>
				    		<div class="col-sm-4">
				    		</div>
				    	';
				    
				    } else {

						echo '
				    		<div class="col-sm-4">
				    		</div>	
							<div class="col-sm-4">
							<br><br>
								<img src="../photos/avail_pawon.png">;		
							</div>
				    		<div class="col-sm-4">
				    		</div>';			
					}
				} catch (Exception $e) {
					echo $e;
				}
			?>
		</div>
	</div>
</div>