<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="index.php">Dashboard</a>
  </li>
  <li class="breadcrumb-item">
    <a href="index.php?halaman=manage_ruangan">Management Rooms</a>
  </li>
  <li class="breadcrumb-item active">Insert Rooms</li>
</ol>
<br>
<h3><center>Insert Rooms</center></h3>
<br>
<br>
<div class="row">
    <div class="col-sm-2">
    
    </div>
    <div class="col-sm-8">
        <form action="../config/doInsertrooms.php" class="form-horizontal" method="POST" enctype="multipart/form-data">
            <div class="form-group">
              <label class="control-label">Nama Ruangan :</label>
                <div>
                    <input type="text" class="form-control" name="nama" placeholder="Nama Ruangan">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Tata Letak :</label>
                <div>
                    <input class="form-control" type="file" name="fileToUpload" id="fileToUpload">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Kapasitas :</label>
                <div>
                    <input type="text" class="form-control" name="kapasitas" placeholder="Kapasitas">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Fasilitas :</label>
                <div>
                    <textarea type="text" class="form-control" name="kapasitas" placeholder="Kapasitas">
                      
                    </textarea>
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="col-sm-12" align="right">
                    <button class="btn btn-danger" type="submit">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>