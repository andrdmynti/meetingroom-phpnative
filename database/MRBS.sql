-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 31 Okt 2018 pada 20.34
-- Versi server: 10.1.32-MariaDB
-- Versi PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `MRBS`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `reservation`
--

CREATE TABLE `reservation` (
  `id_reservation` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `rapat` varchar(50) NOT NULL,
  `divisi` varchar(30) NOT NULL,
  `ruangan` varchar(30) NOT NULL,
  `tanggal` varchar(10) NOT NULL,
  `start` text NOT NULL,
  `finish` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `reservation`
--

INSERT INTO `reservation` (`id_reservation`, `nama`, `rapat`, `divisi`, `ruangan`, `tanggal`, `start`, `finish`) VALUES
(7, 'Nama', 'Jenis Rapat', 'F', 'Prambanan', '2018-10-11', '12:00', '15:42'),
(8, 'nama', 'rapat', 'asda', 'Borobudur', '2018-10-11', '12:35', '15:50'),
(9, '', '', '', 'Mendut', '2018-10-11', '12:10', '19:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruangan`
--

CREATE TABLE `ruangan` (
  `id_ruangan` int(11) NOT NULL,
  `nama_ruangan` varchar(50) NOT NULL,
  `tata_letak` text NOT NULL,
  `kapasitas` int(20) NOT NULL,
  `fasilitas` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruangan`
--

INSERT INTO `ruangan` (`id_ruangan`, `nama_ruangan`, `tata_letak`, `kapasitas`, `fasilitas`) VALUES
(1, 'Board Room - 1', '../uploads/layoout_board.png', 22, '<p>Projector</p>\r\n<p>LED TV</p>\r\n<p>Whiteboard</p>\r\n<p>Video Confrence</p>\r\n<p>Microphone</p>\r\n<p>Multimedia</p>'),
(2, 'Board Room - 2', '../uploads/layoout_board.png', 22, '<p>Projector</p>\r\n<p>LED TV</p>\r\n<p>Whiteboard</p>\r\n<p>Video Confrence</p>\r\n<p>Microphone</p>\r\n<p>Multimedia</p>'),
(3, 'PPP - 1', '../uploads/layoout_ppp.png', 20, '<p>Projector</p>\r\n<p>LED TV</p>\r\n<p>Whiteboard</p>\r\n<p>Video Confrence</p>\r\n<p>Microphone</p>\r\n<p>Multimedia</p>'),
(4, 'PPP - 2', '../uploads/layoout_ppp.png', 20, '<p>Projector</p>\r\n<p>LED TV</p>\r\n<p>Whiteboard</p>\r\n<p>Video Confrence</p>\r\n<p>Microphone</p>\r\n<p>Multimedia</p>'),
(5, 'PPP - 3', '../uploads/layoout_ppp.png', 20, '<p>Projector</p>\r\n<p>LED TV</p>\r\n<p>Whiteboard</p>\r\n<p>Video Confrence</p>\r\n<p>Microphone</p>\r\n<p>Multimedia</p>'),
(6, 'PPP - 4', '../uploads/layoout_ppp.png', 20, '<p>Projector</p>\r\n<p>LED TV</p>\r\n<p>Whiteboard</p>\r\n<p>Video Confrence</p>\r\n<p>Microphone</p>\r\n<p>Multimedia</p>'),
(7, 'Mezzanine - 1', '../uploads/layoout_mezzanine.png', 12, '<p>Projector</p>\r\n<p>LED TV</p>\r\n<p>Whiteboard</p>\r\n<p>Microphone</p>'),
(8, 'Mezzanine - 2', '../uploads/layoout_mezzanine.png', 12, '<p>Projector</p>\r\n<p>LED TV</p>\r\n<p>Whiteboard</p>\r\n<p>Microphone</p>'),
(9, 'Mezzanine - 3', '../uploads/layoout_mezzanine.png', 12, '<p>Projector</p>\r\n<p>LED TV</p>\r\n<p>Whiteboard</p>\r\n<p>Microphone</p>'),
(10, 'Borobudur', '../uploads/layoout_medium.png', 8, '<p>LED TV</p>\r\n<p>Whiteboard</p>'),
(11, 'Prambanan', '../uploads/layoout_medium.png', 8, '<p>LED TV</p>\r\n<p>Whiteboard</p>'),
(12, 'Mendut', '../uploads/layoout_small.png', 4, '<p>LED TV</p>\r\n<p>Whiteboard</p>'),
(13, 'Pawon', '../uploads/layoout_small.png', 4, '<p>LED TV</p>\r\n<p>Whiteboard</p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `nama`, `jabatan`, `username`, `password`, `level`) VALUES
(1, 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(2, 'pegawai', '', 'pegawai', '047aeeb234644b9e2d4138ed3bc7976a', 'pegawai');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id_reservation`);

--
-- Indeks untuk tabel `ruangan`
--
ALTER TABLE `ruangan`
  ADD PRIMARY KEY (`id_ruangan`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id_reservation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `ruangan`
--
ALTER TABLE `ruangan`
  MODIFY `id_ruangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
