<?php

	//error_reporting();
	include 'config/connection.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>MRBS</title>
		<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
	  	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
	  	<script src="assets/js/bootstrap.min.js"></script>
</head>
<body style="background-color: black;">
	<div class="container">
		<br><br><br><br><br><br><br>
		<div class="row">
			<div class="col-sm-2">
				
			</div>
			<div class="col-sm-8">
				<div class="card">
				    <div class="card-header">Sign in to MRBS</div>
				    <div class="card-body">
				    	<form action="config/doLogin.php" method="POST">
				    		<div class="form-group row">
				    	  		<label class="col-3 col-form-label">Username</label>
				    			<label class="col-1 col-form-label">:</label>
				    			<div class="col-6">
				    				<input class="form-control" name="username" type="text" placeholder="Username">
				    			</div>
				    		</div>
				    	    <div class="form-group row">
				    			<label class="col-3 col-form-label">Password</label>
				    			<label class="col-1 col-form-label">:</label>
				    			<div class="col-6">
				    				<input class="form-control" name="password" type="password" placeholder="Password">
				    			</div>
				    		</div>
				    		<div class="form-group row">
				    		    <label class="col-3 col-form-label"></label>
				    		    <label class="col-1 col-form-label"></label>
				    		    <div class="col-6" align="Right">
				    		    	<button class="btn btn-danger">Sign in</button>
				    			</div>
				    		</div>
				    	</form>
				    </div>
				</div>	
			</div>
			<div class="col-sm-2">
				
			</div>
		</div>
	</div>
</body>
</html>